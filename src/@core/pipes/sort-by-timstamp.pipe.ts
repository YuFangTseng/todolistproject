import { PipeTransform, Pipe } from '@angular/core';
import { ToDoListItem } from '@core/types';

@Pipe({
	name: 'sortByTimestamp'
})
export class SortByTimestampPipe implements PipeTransform {

    /**
     * transform function
     * 
     * @param {Array<ToDoListItem>} input iterating Items
     * @param {string} config "+" : Ascending, "-" : Descending 
     * 
     * @returns {Array<ToDoListItem>}
     */
	transform(input: Array<ToDoListItem>, config : string = '+') : Array<ToDoListItem> {

        return input.sort( function(a:ToDoListItem, b:ToDoListItem) {
            
            // Compare First
            let order = a.timestamp - b.timestamp;
            
            return config === '-' ? -order : order;
        });
    }
}