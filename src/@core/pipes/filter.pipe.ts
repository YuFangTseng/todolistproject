import { Pipe, PipeTransform } from '@angular/core';
import { ToDoListItem } from '@core/types';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
  
  /**
   * Transform
   *
   * @param {ToDoListItem[]} items
   * @param {string} searchText
   *
   * @returns {Array<ToDoListItem>}
   */
  transform(items: Array<ToDoListItem>, searchText: string, status : string) : Array<ToDoListItem> {
    
    // Prevent Item and searchText Undefined
    if (!items) return [];
    if (!searchText && !status) return items;

    // Convert Key to UpperCase
    searchText = searchText.toUpperCase();

    return items.filter((it : ToDoListItem) => {

      let tempFilter = it.description.toUpperCase().includes(searchText);
      
      if(status)
        return status == "Completed" ? tempFilter && it.status : tempFilter && !it.status;
      else 
        return tempFilter;

    });
  }
}
