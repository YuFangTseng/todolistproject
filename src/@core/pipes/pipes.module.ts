import { NgModule } from '@angular/core';

/* Sort Pipe */
import { SortByTimestampPipe } from './sort-by-timstamp.pipe';

/* Filter Pipe */
import { FilterPipe } from './filter.pipe';
import { HighlightPipe } from './high-light.pipe';


@NgModule({
  declarations: [
    SortByTimestampPipe,
    FilterPipe,
    HighlightPipe
  ],
  exports: [
    SortByTimestampPipe,
    FilterPipe,
    HighlightPipe
  ]
})
export class PipesModule {}
