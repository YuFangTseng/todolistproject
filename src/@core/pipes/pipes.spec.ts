import { ToDoListItem } from '@core/types';
import { SortByTimestampPipe } from './sort-by-timstamp.pipe';
import { HighlightPipe } from './high-light.pipe';
import { FilterPipe } from './filter.pipe';

describe('Custom Pipe Testing', () => {
    
    describe('SortByTimestampPipe', () => {

        let pipe : SortByTimestampPipe;
        const testData : Array<ToDoListItem> = [{
            id : "id1",
            status : false,
            timestamp : 1,
            description : "description",
            modifyFlag : false
        },
        {
            id : "id3",
            status : false,
            timestamp : 3,
            description : "description",
            modifyFlag : false
        },
        {
            id : "id2",
            status : false,
            timestamp : 2,
            description : "description",
            modifyFlag : false
        }];

        // Inital the pipe class
        beforeEach(async () => {
            pipe = new SortByTimestampPipe();
        });

        // Should create an instance
        it('# should create an instance', () => {
            expect(pipe).toBeTruthy();
        });

        // Without Config Should return an ASC array
        it('# Without Config Should return an ASC array', () => {
            
            // Run Transform
            const result = pipe.transform(testData);

            expect(result).toEqual([{
                id : "id1",
                status : false,
                timestamp : 1,
                description : "description",
                modifyFlag : false
            },
            {
                id : "id2",
                status : false,
                timestamp : 2,
                description : "description",
                modifyFlag : false
            },
            {
                id : "id3",
                status : false,
                timestamp : 3,
                description : "description",
                modifyFlag : false
            }]);

        });

        // Config "+" Should return an ASC array
        it('# Config "+" Should return an ASC array', () => {
            
            // Run Transform
            const result = pipe.transform(testData,'+');

            expect(result).toEqual([{
                id : "id1",
                status : false,
                timestamp : 1,
                description : "description",
                modifyFlag : false
            },
            {
                id : "id2",
                status : false,
                timestamp : 2,
                description : "description",
                modifyFlag : false
            },
            {
                id : "id3",
                status : false,
                timestamp : 3,
                description : "description",
                modifyFlag : false
            }]);

        });

        // Config "-" Should return an DES array
        it('# Config "-" Should return an DES array', () => {
            
            // Run Transform
            const result = pipe.transform(testData,'-');

            expect(result).toEqual([{
                id : "id3",
                status : false,
                timestamp : 3,
                description : "description",
                modifyFlag : false
            },
            {
                id : "id2",
                status : false,
                timestamp : 2,
                description : "description",
                modifyFlag : false
            },
            {
                id : "id1",
                status : false,
                timestamp : 1,
                description : "description",
                modifyFlag : false
            }]);

        });

    });

    describe('HighlightPipe', () => {

        let pipe : HighlightPipe;

        // Inital the pipe class
        beforeEach(async () => {
            pipe = new HighlightPipe();
        });

        // Should create an instance
        it('# Should create an instance', () => {
            expect(pipe).toBeTruthy();
        });

        // Should return same word when Empty Search 
        it('# Should return same word when Empty Search', () => {

            // Run Transform
            const result = pipe.transform(`ABC`,'');
            
            expect(result).toBe(`ABC`);
        });

        // Should pass single match
        it('# Should pass single match', () => {

            // Run Transform
            const result = pipe.transform(`ABC`,'ABC');

            expect(result).toBe(`<span class="text-highlight">ABC</span>`);
        });

        // Should pass multiple match
        it('# Should pass multiple match', () => {

            // Run Transform
            const result = pipe.transform(`ABCABC`,'ABC');

            expect(result).toBe(`<span class="text-highlight">ABC</span><span class="text-highlight">ABC</span>`);
        });

        // Should accept the special character except <>
        it('# Should accept the special character except <>', () => {

            // Run Transform
            const result = pipe.transform(`[*()[]\\?+-]ABC[*()[]\\?+-]`,'[*()[]\\?+-]ABC[*()[]\\?+-]');
            
            expect(result).toBe(`<span class="text-highlight">[*()[]\\?+-]ABC[*()[]\\?+-]</span>`);
        });

        // Should filter out HTML Tag and replace with " "
        it('# Should filter out HTML Tag with " "', () => {

            // Run Transform
            const result = pipe.transform(`<script>alert()</script>ABC`,'ABC');
            
            expect(result).toBe(` alert() <span class="text-highlight">ABC</span>`);
        });

    });
    
    describe('FilterPipe', () => {

        let pipe : FilterPipe;
        const testData : Array<ToDoListItem> = [{
            id : "id1",
            status : true,
            timestamp : 1,
            description : "descriptionA",
            modifyFlag : false
        },
        {
            id : "id3",
            status : false,
            timestamp : 3,
            description : "descriptionB",
            modifyFlag : false
        },
        {
            id : "id2",
            status : false,
            timestamp : 2,
            description : "descriptionA",
            modifyFlag : false
        }];

        // Inital the pipe class
        beforeEach(async () => {
            pipe = new FilterPipe();
        });

        // Should create an instance
        it('# Should create an instance', () => {
            expect(pipe).toBeTruthy();
        });

        // Should return same array when searchText and status are all empty
        it('# Should return same array when searchText and status are all empty', () => {
            
             // Run Transform
             const result = pipe.transform(testData,"","");
            
             expect(result).toEqual(testData);
        });

        // Should return description filter array when searchText not empty
        it('# Should return description filter array when searchText not empty', () => {
            
            // Run Transform
            const result = pipe.transform(testData,"descriptionA","");
           
            expect(result).toEqual([{
                id : "id1",
                status : true,
                timestamp : 1,
                description : "descriptionA",
                modifyFlag : false
            },
            {
                id : "id2",
                status : false,
                timestamp : 2,
                description : "descriptionA",
                modifyFlag : false
            }]);
        });

        // Should return status:true filter array when status set "Completed"
        it('# Should return status:true filter array when status set "Completed"', () => {
            
            // Run Transform
            const result = pipe.transform(testData,"","Completed");
           
            expect(result).toEqual([{
                id : "id1",
                status : true,
                timestamp : 1,
                description : "descriptionA",
                modifyFlag : false
            }]);
        });

        // Should return status:false filter array when status not set "Completed"
        it('# Should return status:false filter array when status not set "Completed"', () => {
            
            // Run Transform
            const result = pipe.transform(testData,"","UnCompleted");
           
            expect(result).toEqual([{
                id : "id3",
                status : false,
                timestamp : 3,
                description : "descriptionB",
                modifyFlag : false
            },
            {
                id : "id2",
                status : false,
                timestamp : 2,
                description : "descriptionA",
                modifyFlag : false
            }]);
        });

        // Should return both filter array when both are set
        it('# Should return both filter array when both are set', () => {
            
            // Run Transform
            const result = pipe.transform(testData,"descriptionB","UnCompleted");
           
            expect(result).toEqual([{
                id : "id3",
                status : false,
                timestamp : 3,
                description : "descriptionB",
                modifyFlag : false
            }]);
        });

    });
    
});
