import { ToDoListItem } from "@core/types";

export const toDoList : Array<ToDoListItem> = [
    { id : "_1", status : false, timestamp : 123, description : "Use any frontend frameworks of your choice(Angular, React, Vue, etc)"},
    { id : "_2", status : false, timestamp : 524, description : "Feel free to build the UI in your own way"},
    { id : "_3", status : false, timestamp : 125, description : "Can add tasks with a short description"},
    { id : "_4", status : true, timestamp : 126, description : "Show a list of all tasks(completed/uncompleted)"},
    { id : "_5", status : true, timestamp : 127, description : "Can set a task as completed or uncompleted"},
    { id : "_6", status : true, timestamp : 628, description : "Can delete a task"},
    { id : "_7", status : true, timestamp : 129, description : "Can update the task description"},
    { id : "_8", status : false, timestamp : 220, description : "Can sort tasks by time the tasks are created"},
    { id : "_9", status : false, timestamp : 131, description : "Can filter or search tasks"},
    { id : "_0", status : false, timestamp : 132, description : "Make the UI responsive"},
    { id : "_11", status : false, timestamp : 139, description : "List Longer"},
    { id : "_12", status : false, timestamp : 534, description : "Source code: public repo on github"},
    { id : "_13", status : false, timestamp : 3453, description : "Documentation on how to run"},
    { id : "_14", status : false, timestamp : 132345, description : "Documentation on codebase"},
    { id : "_15", status : false, timestamp : 13342, description : "Live demo site"},
    { id : "_16", status : true, timestamp : 213432, description : "Live demo site(1)"},
    { id : "_17", status : true, timestamp : 2352, description : "Live demo site(2)"},
    { id : "_18", status : false, timestamp : 2368, description : "Live demo site(3)"},
    { id : "_19", status : false, timestamp : 6795, description : "Live demo site(4)"}
    
];