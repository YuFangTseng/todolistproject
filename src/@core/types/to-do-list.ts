export interface ToDoListItem {
    id          : string;
    status      : boolean;
    timestamp   : number;
    description : string;
    modifyFlag? : boolean;
};