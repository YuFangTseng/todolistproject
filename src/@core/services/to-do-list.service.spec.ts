import { TestBed } from '@angular/core/testing';

/******* Core Resourse *******/
import { ToDoListService } from '@core/services/to-do-list.service';
import { toDoList } from '@core/constant';
import { ToDoListItem } from '@core/types';


describe('ToDoListService', () => {

    let service: ToDoListService;

    // Set Up For the Test
    beforeEach(async () => {

        await TestBed.configureTestingModule({
            providers : [
                ToDoListService
            ]
        }).compileComponents();
    });

    // Render Testing
    describe(`Render Testing`, () => {

        // Initial service
        beforeEach(async () => {
            service = TestBed.inject(ToDoListService);
        });

        //  should create the Service
        it('# should create the Service', () => {
            expect(service).toBeTruthy();
        });

        //  Data should be initial
        it('# Data should be initial', () => {
            expect(service['currentToDoListSubject']).toBeDefined();
            expect(service['currentToDoListSubject'].value).toEqual(toDoList);
            expect(service.currentToDoList).toEqual(service['currentToDoListSubject'].asObservable());
        });

    });

    // Function Testing
    describe(`Function Testing`, () => {

        // Initial service
        beforeEach(async () => {
            service = TestBed.inject(ToDoListService);
        });

        // currentToDoListValue() : getter should return current To Do Items List
        it('# currentToDoListValue() : getter should return current To Do Items List', () => {

            // get value
            const value = service.currentToDoListValue;

            // value should be behavior value
            expect(value).toEqual(service['currentToDoListSubject'].value);
        });

        // queryToDoList() : should get toDoList from const
        it('# queryToDoList() : should get toDoList from const', () => {

            service.currentToDoList.subscribe((data : Array<ToDoListItem>) => {
                expect(data).toEqual(toDoList);
            });

            // trigger Function
            service['queryToDoList']();
        });

        // refreshData() : should call queryToDoList function
        it('# refreshData() : should call queryToDoList function', () => {

            // spy on queryToDoList in service
            let spy = spyOn<any>(service, 'queryToDoList');

            // trigger Function
            service.refreshData();

            // except queryToDoList call one
            expect(spy).toHaveBeenCalledTimes(1);
        });

        // createToDo() : should add To Do Item to the list
        it('# createToDo() : should add To Do Item to the list', () => {

            // Prepare New Data
            const DataObj = new Date();
            const newData = {
                id : `_${DataObj.getTime()}`,
                status : false,
                description : "abcdef",
                timestamp : DataObj.getTime(),
                modifyFlag : false
            };

            // Call create function
            service.createToDo(newData);

            // except item is in the list
            expect(service.currentToDoListValue.findIndex((element : ToDoListItem) => element.id == newData.id)).toBeGreaterThanOrEqual(0);
        });

        // updateToDo() : should update To Do Item in the list
        it('# updateToDo() : should update To Do Item in the list', () => {

            // Prepare New Data
            const DataObj = new Date();
            const newData = {
                id : `_${DataObj.getTime()}`,
                status : false,
                description : "abcdef",
                timestamp : DataObj.getTime(),
                modifyFlag : false
            };

            // Call create function
            service.createToDo(newData);

            // Prepare Update Data
            const updateData = {
                id : newData.id,
                status : true,
                description : "abcdef",
                timestamp : newData.timestamp,
                modifyFlag : true
            };

            // Call update function
            service.updateToDo(updateData);

            // get item
            const Item = service.currentToDoListValue[service.currentToDoListValue.findIndex((element : ToDoListItem) => element.id == newData.id)];

            // Check all update value
            expect(Item).toEqual(updateData);
        });

        // deleteToDo() : should delete To Do Item from the list
        it('# deleteToDo() : should delete To Do Item from the list', () => {

            // Prepare New Data
            const DataObj = new Date();
            const newData = {
                id : `_${DataObj.getTime()}`,
                status : false,
                description : "abcdef",
                timestamp : DataObj.getTime(),
                modifyFlag : false
            };

            // Call create function
            service.createToDo(newData);

            // Call delete function
            service.deleteToDo(newData);
            
            // except item is not in the list
            expect(service.currentToDoListValue.findIndex((element : ToDoListItem) => element.id == newData.id)).toBeLessThan(0);
        });
        
    })

})
