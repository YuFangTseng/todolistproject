/******* Angular Resourse *******/
import { Injectable } from '@angular/core';

/******* Core Resourse *******/
import { toDoList } from '@core/constant';
import { ToDoListItem } from '@core/types';

/******* Plug-In Resourse *******/
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class ToDoListService {

    // Public

    // List Data as Observable
    public currentToDoList: Observable<Array<ToDoListItem>>;

    // Private

    // List Data as Behavior Subject
    private currentToDoListSubject: BehaviorSubject<Array<ToDoListItem>>;

    /**
     * Constructor
     */
    constructor() {
        
        // List Initial
        this.currentToDoListSubject = new BehaviorSubject<Array<ToDoListItem>>([]);
        this.currentToDoList = this.currentToDoListSubject.asObservable();
        
        // Query To Do List Data
        this.queryToDoList();

    }

    //  Accessors
    // -----------------------------------------------------------------------------------------------------

    // getter: To Do List value
    public get currentToDoListValue() : Array<ToDoListItem> {
        return this.currentToDoListSubject.value;
    }

    // Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Query To Do List
     */
    private queryToDoList() : void {
        
        // Get Data from resource
        this.currentToDoListSubject.next([...toDoList]); 
    }
    
    // Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * refresh To Do List
     */
    public refreshData() : void {
        this.queryToDoList();
    }

    /**
     * delete To Do List
     * 
     * @param {ToDoListItem} deleteItem
     */
    public deleteToDo(deleteItem : ToDoListItem) : void {

        // Get Current List
        let list = this.currentToDoListValue;

        // Iterate and return the Index
        const index = list.findIndex((element : ToDoListItem) => element.id == deleteItem.id);
        
        // Delete item from the array
        list.splice(index, 1);

        // Move to next status
        this.currentToDoListSubject.next([...list]);
    }

    /**
     * Update the To Fo Item
     * 
     * @param {ToDoListItem} editItem
     */
    public updateToDo(editItem : ToDoListItem) : void {

        // Get Current List
        let list = this.currentToDoListValue;

        // Iterate and return the Index
        const index = list.findIndex((element : ToDoListItem) => element.id == editItem.id);

        // Replace item from the array
        list[index] = editItem;

        // Move to next status
        this.currentToDoListSubject.next([...list]);
    }

    /**
     * Update the To Fo Item
     * 
     * @param {ToDoListItem} newItem
     */
    public createToDo(newItem : ToDoListItem) : void {

        // Get Current List
        let list = this.currentToDoListValue;

        // Add item to the array
        list.push(newItem);

        // Move to next status
        this.currentToDoListSubject.next([...list]);
    }
}