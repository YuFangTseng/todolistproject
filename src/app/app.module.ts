/******* Angular Resourse *******/
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';

/******* Core Resourse *******/
import { CoreCommonModule } from '@core/common.module';

/******* Plug-In Resourse *******/
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

/******* Components Resourse *******/
import { AppComponent } from './app.component';

/******* Module Resourse *******/
import { LayoutModule } from './layout/layout.module';

// App Route Settings
const appRoutes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/to-do-list/to-do-list.module').then(m => m.ToDiListModule)
  },
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,

    // Routing Module
    RouterModule.forRoot(appRoutes),

    // Common Module
    CoreCommonModule,
    
    // NG Bootstrap Module
    NgbModule,

    // Module Resource
    LayoutModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
