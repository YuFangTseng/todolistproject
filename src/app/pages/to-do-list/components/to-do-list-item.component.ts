
/******* Angular Resourse *******/
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewEncapsulation } from '@angular/core';

/******* Core Resourse *******/
import { ToDoListItem } from '@core/types';

@Component({
  selector: 'to-do-list-item',
  templateUrl: './to-do-list-item.component.html',
  encapsulation: ViewEncapsulation.None
})
export class ToDoListItemComponent implements OnInit, OnDestroy{
  
    // Public

    // @Input() ToDo Data
    @Input('data') ToDo: ToDoListItem = {
      id: '',
      status: false,
      timestamp: 0,
      description: ''
    };

    // @Input() ToDo Data
    @Input('Search_Key') Key: string = "";

    // @Output Edit the ToDo Data
    @Output('editToDo') _editToDo: EventEmitter<ToDoListItem> = new EventEmitter();

    // @Output Toggle ToDo Status
    @Output('toggleToDo') _toggleToDo: EventEmitter<ToDoListItem> = new EventEmitter();

    // @Output Delete the ToDo Data
    @Output('deleteToDo') _deleteToDo: EventEmitter<ToDoListItem> = new EventEmitter();

    // Private

    /**
     * Constructor
     */
    constructor() {}

    //  Accessors
    // -----------------------------------------------------------------------------------------------------

    // Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {}

    /**
     * On Destroy
     */
    ngOnDestroy(): void {}

    // Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Prevent Default and stop Propagation
     * 
     * @param {any} event 
     */
    private preventDefaultClickEvent(event : any) : void {

      // Prevent Event Default
      event.preventDefault();

      // Stop Event Propagation
      event.stopPropagation();

    }

    // Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Edit this Todo Item
     * 
     * @param {any} event
     */
    public editItem(event : any) : void {

      this.preventDefaultClickEvent(event);

      // Get Item
      let Item = this.ToDo;

      // Swithc Modify On
      Item.modifyFlag = true;

      // emit the edit Item 
      this._editToDo.emit(Item);
    }

    /**
     * Delete this Todo Item
     * 
     * @param {any} event
     */
    public deleteItem(event : any) : void {
      
      this.preventDefaultClickEvent(event);

      // emit the delete Item 
      this._deleteToDo.emit(this.ToDo);
    }

    /**
     * Toogle this Todo Item's Completed Status
     * 
     * @param {any} event
     */
    public toggleStatus(event : any) : void {
      
      this.preventDefaultClickEvent(event);

      // Get Item
      let Item = this.ToDo;

      // Toggle the Status
      Item.status = !Item.status;

      // emit the Toggle  
      this._toggleToDo.emit(Item);
    }

}   
