
/******* Angular Resourse *******/
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

/******* Core Resourse *******/
import { ToDoListItem } from '@core/types';

@Component({
  selector: 'to-do-list-item-form',
  templateUrl: './to-do-list-item-form.component.html',
  encapsulation: ViewEncapsulation.None
})
export class ToDoListItemFormComponent implements OnInit, OnDestroy{
  
    // Public

    // @Input() ToDo Data
    @Input('data') ToDo: ToDoListItem = {
      id: '',
      status: false,
      timestamp: 0,
      description: ''
    };

    // @Output Save the ToDo Data
    @Output('saveToDo') _saveToDo: EventEmitter<ToDoListItem> = new EventEmitter();

    // @Output Edit ToDo Data
    @Output('cancelToDo') _cancelToDo: EventEmitter<ToDoListItem> = new EventEmitter();

    // To Do Form 
    public toDoForm: FormGroup;

    // Submit flag
    public submitted: boolean = false;


    // Private

    /**
     * Constructor
     * 
     * @param {FormBuilder} _formBuilder
     */
    constructor(private _formBuilder : FormBuilder) {

      // initial the To Do Form
      this.toDoForm = this._formBuilder.group({
        description : ['', [Validators.required]]
      });

    }

    //  Accessors
    // -----------------------------------------------------------------------------------------------------

    /**
     * form getter 
     * convenience getter for easy access to form fields
     */
    public get toDoFormControls() {
      return this.toDoForm.controls;
    }


    // Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        
        // render description
        this.toDoForm.patchValue({description : this.ToDo.description})
    }

    /**
     * On Destroy
     */
    ngOnDestroy(): void {}

    // Private methods
    // -----------------------------------------------------------------------------------------------------

    // Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Submit the To Do Form
     */
    public onSubmit() {
      
      // set submitted Flag
      this.submitted = true;

      // stop here if form is invalid
      if (this.toDoForm.invalid) {
        return;
      }

      // inital the Date Object
      const dateObj = new Date();

      // form input
      let form : ToDoListItem = {
        id : this.ToDo.id ? this.ToDo.id : `_${dateObj.getTime()}`,
        status : false,
        description : this.toDoForm.value.description.replace(/<(?:.|\n)*?>/gm, ' '),
        timestamp : this.ToDo.timestamp > 0 ? this.ToDo.timestamp : dateObj.getTime(),
      };

      // emit save event
      this._saveToDo.emit(form);

      // Reset the form
      this.toDoForm.reset();

      // Rest the submitted Flag
      this.submitted = false;
    }

    /**
     * Cancel Edit To Do Form
     */
    public cancel() {
        
        // Get Item
        let Item = this.ToDo;

        // Swithc Modify Off
        Item.modifyFlag = false;

        // emit save event
        this._cancelToDo.emit(Item);

        // Reset the form
        this.toDoForm.reset();

        // Rest the submitted Flag
        this.submitted = false;
    }


}   
