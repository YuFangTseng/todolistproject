/******* Angular Resourse *******/
import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';

/******* Core Resourse *******/
import { ToDoListService } from '@core/services/to-do-list.service';
import { ToDoListItem } from '@core/types';

/******* Plug-In Resourse *******/
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'to-do-list',
  templateUrl: './to-do-list.component.html',
  styleUrls: ['./to-do-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ToDoListComponent implements OnInit, OnDestroy{
  
    // Public

    // To Do List Items
    public toDoListItems : Array<ToDoListItem> = [];

    // Search Model
    public Search_Key : string = "";

    // Search Status Model
    public Search_Status : string = "";

    // Sorting Direction
    public Sorting_Dir : boolean = false;

    // New Item Flag
    public newToDoFlag : boolean = false;

    // Private

    // Unsubscribe Subject
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     * 
     * @param {ToDoListService} _ToDoListService
     */
    constructor(private _ToDoListService : ToDoListService) {

      // Set the private defaults
      this._unsubscribeAll = new Subject();

    }

    //  Accessors
    // -----------------------------------------------------------------------------------------------------

    /**
     * Sorting Direction 
     */
    public get SortingDir() : string {
      return this.Sorting_Dir ? "ASC" : "DES";
    }

    // Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
      
      // Subscribe Todo List Data
      this._ToDoListService.currentToDoList.pipe(takeUntil(this._unsubscribeAll)).subscribe(
        (toDoListData : Array<ToDoListItem>)=>{
          // re-Assign the array
          this.toDoListItems = [...toDoListData];
        }
      );

    }

    /**
     * On Destroy
     */
    ngOnDestroy(): void {
      
      // Unsubscribe from all subscriptions
      this._unsubscribeAll.next(0);
      this._unsubscribeAll.complete();
    }

    // Private methods
    // -----------------------------------------------------------------------------------------------------

    // Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * toggle Sorting Dir
     */
    public toggleSortingDir() {

      // toggle Sorting Dir
      this.Sorting_Dir = !this.Sorting_Dir;
    }

    /**
     * delete the To Do Item
     * 
     * @param {ToDoListItem} ToDoItem item to be deleted
     */
    public deleteToDo(ToDoItem : ToDoListItem) : void {
      
      // Delete the To Do
      this._ToDoListService.deleteToDo(ToDoItem);
    }

    /**
     * toggle the To Do Item's Status
     * 
     * @param {ToDoListItem} ToDoItem item to be update
     */
    public toggleToDo(ToDoItem : ToDoListItem) : void {

      // Update the To Do
      this._ToDoListService.updateToDo(ToDoItem);
    }

    /**
     * Update the To Do Item
     * 
     * @param {ToDoListItem} ToDoItem item to be edited
     */
    public updateToDo(ToDoItem : ToDoListItem) : void {
      
      // Update the To Do
      this._ToDoListService.updateToDo(ToDoItem);
    }

    /**
     * Create the To Do Item
     * 
     * @param {ToDoListItem} ToDoItem item to be created
     */
    public createToDo(ToDoItem : ToDoListItem) : void {

      // Create the To Do
      this._ToDoListService.createToDo(ToDoItem);

      // Set Flag to Falsy
      this.newToDoFlag = false;
    }

    /**
     * Cancel the Creating To Do Form
     */
    public cancelCreatingToDo() : void {

      // Set Flag to Falsy
      this.newToDoFlag = false;
    }

}   
