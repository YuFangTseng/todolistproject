import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

/******* Core Resourse *******/
import { CoreCommonModule } from '@core/common.module';
import { ToDoListService } from '@core/services/to-do-list.service';

/******* Component Resourse *******/
import { ToDoListComponent } from "./to-do-list.component";
import { ToDoListItemComponent } from './components/to-do-list-item.component';
import { ToDoListItemFormComponent } from './components/to-do-list-item-form.component';
import { By } from '@angular/platform-browser';
import { ToDoListItem } from '@core/types';

describe('ToDoListComponent - <to-do-list>', () => {
    
    let component: ToDoListComponent;
    let fixture: ComponentFixture<ToDoListComponent>;
    let FakeService : ToDoListService;

    // Set Up For the Test
    beforeEach(async () => {

        await TestBed.configureTestingModule({
        imports: [
            RouterTestingModule,
            CoreCommonModule
        ],
        declarations: [
            ToDoListComponent,
            ToDoListItemComponent,
            ToDoListItemFormComponent
        ],
        providers : [
            ToDoListService
        ]
        }).compileComponents();

    });

    // Render Testing
    describe(`Render Testing`, () => {
        
        // Inital the component to be tested
        beforeEach(async () => {
            fixture = TestBed.createComponent(ToDoListComponent);
            FakeService = TestBed.inject(ToDoListService);
            component = fixture.componentInstance;
            fixture.detectChanges();
        });

        // Component should be created
        it('# should create the component', () => {
            expect(component).toBeTruthy();
        });

        // Component Data initial
        it('# should initial the Data', () => {
            
            // Check All Inital value
            expect(component.Search_Key).toBe("");
            expect(component.Search_Status).toBe("");
            expect(component.Sorting_Dir).toBeFalse();
            expect(component.newToDoFlag).toBeFalse();
            expect(component.toDoListItems).toBeDefined();
            expect(component['_ToDoListService']).toBeDefined();
            expect(component['_unsubscribeAll']).toBeDefined();
        });
        
        describe(`Header Section`, () => {

            // Sorting Button : should  display "Sort By Timestamp - ASC" when Sorting_Dir is true'
            it('# Sorting Button : should  display "Sort By Timestamp - ASC" when Sorting_Dir is true', () => {

                // Get Button
                const BUTTON = fixture.debugElement.nativeElement.querySelector('button.sortByTimeStamp');

                // Set Sorting_Dir to True 
                component.Sorting_Dir = true;
                fixture.detectChanges();

                // Button text is - ACS
                expect(BUTTON.textContent).toContain("Sort By Timestamp - ASC");
            });

            // Sorting Button : should  display "Sort By Timestamp - DES" when Sorting_Dir is false'
            it('# Sorting Button : should  display "Sort By Timestamp - DES" when Sorting_Dir is false', () => {

                // Get Button
                const BUTTON = fixture.debugElement.nativeElement.querySelector('button.sortByTimeStamp');

                // Set Sorting_Dir to false 
                component.Sorting_Dir = false;
                fixture.detectChanges();

                // Button text is - DES
                expect(BUTTON.textContent).toContain("Sort By Timestamp - DES");
            });

        });

        describe(`To Do List Section`, () => {

            // # To Do Item Form : Create Form
            describe(`# To Do Item Form : Create Form`, () => {

                // # To Do Item Form : should display create one when newToDoFlag is true
                it('# To Do Item Form : should display create one when newToDoFlag is true', () => {
                    
                    // Set newToDoFlag to True 
                    component.newToDoFlag = true;
                    fixture.detectChanges();

                    // Get Form
                    const FORM = fixture.debugElement.nativeElement.querySelector('to-do-list-item-form.Create-Form');
                    
                    // Form to be existed
                    expect(FORM).toBeTruthy();
                });

                // # To Do Item Form : should not display create one when newToDoFlag is false
                it('# To Do Item Form : should not display create one when newToDoFlag is false', () => {
                    
                    // Set newToDoFlag to False 
                    component.newToDoFlag = false;
                    fixture.detectChanges();

                    // Get Form
                    const FORM = fixture.debugElement.nativeElement.querySelector('to-do-list-item-form.Create-Form');
                    
                    // Form to be not existed
                    expect(FORM).toBeFalsy();
                });

            });

            // # To Do Item Lsit 
            describe(`# To Do Item Lsit `, () => {
            
                // # To Do Item Lsit  : Render Successfully when toDoListItems is provide
                it('# To Do Item Lsit  : Render Successfully when toDoListItems is provide', () => {
                    
                    // Set toDoListItems having Two Items
                    component.toDoListItems = [{
                        id : "id",
                        status : false,
                        timestamp : 333,
                        description : "description",
                        modifyFlag : false
                    },
                    {
                        id : "id2",
                        status : false,
                        timestamp : 3323,
                        description : "description",
                        modifyFlag : false
                    }];
                    fixture.detectChanges();

                    // Get All list Item
                    let LIST_ITEMS = fixture.debugElement.queryAll(By.css("to-do-list-item"));

                    // Items Existed and length to be 2
                    expect(LIST_ITEMS.length).toBe(2);
                });

                // # To Do Item Lsit  : should display when modifyFlag is false
                it('# To Do Item Lsit  : should display when modifyFlag is false', () => {
                    
                    // Set toDoListItems having Two Items
                    component.toDoListItems = [{
                        id : "id",
                        status : false,
                        timestamp : 333,
                        description : "description",
                        modifyFlag : false
                    }];
                    fixture.detectChanges();

                    // Get All list Item
                    let LIST_ITEMS = fixture.debugElement.queryAll(By.css("to-do-list-item"));

                    // Items Existed and length to be 1
                    expect(LIST_ITEMS.length).toBe(1);
                });

                // # To Do Item Lsit  : should not display when modifyFlag is true
                it('# To Do Item Lsit  : should not display when modifyFlag is false', () => {
                    
                    // Set toDoListItems having Two Items
                    component.toDoListItems = [{
                        id : "id",
                        status : false,
                        timestamp : 333,
                        description : "description",
                        modifyFlag : true
                    }];
                    fixture.detectChanges();

                    // Get All list Item
                    let LIST_ITEMS = fixture.debugElement.queryAll(By.css("to-do-list-item"));

                    // Items not Existed and length to be 0
                    expect(LIST_ITEMS.length).toBe(0);
                });

                // # To Do Item Lsit  : Render ASC toDoListItems by timestamp when Sorting_Dir is true
                it('# To Do Item Lsit  : Render ASC toDoListItems by timestamp when Sorting_Dir is true', () => {
                    
                    const testData = [{
                        id : "id1",
                        status : false,
                        timestamp : 1,
                        description : "description",
                        modifyFlag : false
                    },
                    {
                        id : "id3",
                        status : false,
                        timestamp : 3,
                        description : "description",
                        modifyFlag : false
                    },
                    {
                        id : "id2",
                        status : false,
                        timestamp : 2,
                        description : "description",
                        modifyFlag : false
                    }];

                    // Set toDoListItems and Sorting_Dir to be true
                    component.Sorting_Dir = true;
                    component.toDoListItems = testData;
                    fixture.detectChanges();

                    // Get All list Item
                    let LIST_ITEMS = fixture.debugElement.queryAll(By.css("to-do-list-item"));

                    // Items Match the Order
                    expect(LIST_ITEMS[0].nativeElement.id).toBe('id1');
                    expect(LIST_ITEMS[1].nativeElement.id).toBe('id2');
                    expect(LIST_ITEMS[2].nativeElement.id).toBe('id3');
                });

                // # To Do Item Lsit  : Render DES toDoListItems by timestamp when Sorting_Dir is false
                it('# To Do Item Lsit  : Render DES toDoListItems by timestamp when Sorting_Dir is false', () => {
                    
                    const testData = [{
                        id : "id1",
                        status : false,
                        timestamp : 1,
                        description : "description",
                        modifyFlag : false
                    },
                    {
                        id : "id3",
                        status : false,
                        timestamp : 3,
                        description : "description",
                        modifyFlag : false
                    },
                    {
                        id : "id2",
                        status : false,
                        timestamp : 2,
                        description : "description",
                        modifyFlag : false
                    }];

                    // Set toDoListItems and Sorting_Dir to be false
                    component.Sorting_Dir = false;
                    component.toDoListItems = testData;
                    fixture.detectChanges();

                    // Get All list Item
                    let LIST_ITEMS = fixture.debugElement.queryAll(By.css("to-do-list-item"));

                    // Items Match the Order
                    expect(LIST_ITEMS[0].nativeElement.id).toBe('id3');
                    expect(LIST_ITEMS[1].nativeElement.id).toBe('id2');
                    expect(LIST_ITEMS[2].nativeElement.id).toBe('id1');
                });

                // # To Do Item Lsit : Render All when Search_Status is "" 
                it('# To Do Item Lsit : Render All when Search_Status is ""', () => {
                    
                    const testData = [{
                        id : "id1",
                        status : false,
                        timestamp : 1,
                        description : "description",
                        modifyFlag : false
                    },
                    {
                        id : "id3",
                        status : true,
                        timestamp : 3,
                        description : "description",
                        modifyFlag : false
                    },
                    {
                        id : "id2",
                        status : true,
                        timestamp : 2,
                        description : "description",
                        modifyFlag : false
                    }];

                    // Set toDoListItems and Search_Status to be ""
                    component.Search_Status = "";
                    component.toDoListItems = testData;
                    fixture.detectChanges();

                    // Get All list Item
                    let LIST_ITEMS = fixture.debugElement.queryAll(By.css("to-do-list-item"));

                    // Items Match the Filter
                    expect(LIST_ITEMS.length).toBe(3);
                });

                // # To Do Item Lsit : Render completed list when Search_Status is "Completed" 
                it('# To Do Item Lsit : Render completed list when Search_Status is "Completed"', () => {
                    
                    const testData = [{
                        id : "id1",
                        status : false,
                        timestamp : 1,
                        description : "description",
                        modifyFlag : false
                    },
                    {
                        id : "id3",
                        status : true,
                        timestamp : 3,
                        description : "description",
                        modifyFlag : false
                    },
                    {
                        id : "id2",
                        status : true,
                        timestamp : 2,
                        description : "description",
                        modifyFlag : false
                    }];

                    // Set toDoListItems and Search_Status is "Completed"
                    component.Search_Status = "Completed";
                    component.toDoListItems = testData;
                    fixture.detectChanges();

                    // Get All list Item
                    let LIST_ITEMS = fixture.debugElement.queryAll(By.css("to-do-list-item"));

                    // Items Match the Filter
                    expect(LIST_ITEMS.length).toBe(2);
                });

                // # To Do Item Lsit : Render uncompleted list when Search_Status is "UnCompleted" 
                it('# To Do Item Lsit : Render uncompleted list when Search_Status is "UnCompleted"', () => {
                    
                    const testData = [{
                        id : "id1",
                        status : false,
                        timestamp : 1,
                        description : "description",
                        modifyFlag : false
                    },
                    {
                        id : "id3",
                        status : true,
                        timestamp : 3,
                        description : "description",
                        modifyFlag : false
                    },
                    {
                        id : "id2",
                        status : true,
                        timestamp : 2,
                        description : "description",
                        modifyFlag : false
                    }];

                    // Set toDoListItems and Search_Status to be "UnCompleted"
                    component.Search_Status = "UnCompleted";
                    component.toDoListItems = testData;
                    fixture.detectChanges();

                    // Get All list Item
                    let LIST_ITEMS = fixture.debugElement.queryAll(By.css("to-do-list-item"));

                    // Items Match the Filter
                    expect(LIST_ITEMS.length).toBe(1);
                });

                // # To Do Item Lsit : Render all items when Search_Key is not set 
                it('# To Do Item Lsit : Render all items when Search_Key is not set', () => {
                    
                    const testData = [{
                        id : "id1",
                        status : false,
                        timestamp : 1,
                        description : "descriptionA",
                        modifyFlag : false
                    },
                    {
                        id : "id3",
                        status : true,
                        timestamp : 3,
                        description : "descriptionB",
                        modifyFlag : false
                    },
                    {
                        id : "id2",
                        status : true,
                        timestamp : 2,
                        description : "descriptionA",
                        modifyFlag : false
                    }];

                    // Set toDoListItems and set Search_Key to be "descriptionA"
                    component.Search_Key = "";
                    component.toDoListItems = testData;
                    fixture.detectChanges();

                    // Get All list Item
                    let LIST_ITEMS = fixture.debugElement.queryAll(By.css("to-do-list-item"));

                    // Items Match the Filter
                    expect(LIST_ITEMS.length).toBe(3);
                });

                // # To Do Item Lsit : Render items with description including key when Search_Key is set 
                it('# To Do Item Lsit : Render items with description including key when Search_Key is set', () => {
                    
                    const testData = [{
                        id : "id1",
                        status : false,
                        timestamp : 1,
                        description : "descriptionA",
                        modifyFlag : false
                    },
                    {
                        id : "id3",
                        status : true,
                        timestamp : 3,
                        description : "descriptionB",
                        modifyFlag : false
                    },
                    {
                        id : "id2",
                        status : true,
                        timestamp : 2,
                        description : "descriptionA",
                        modifyFlag : false
                    }];

                    // Set toDoListItems and set Search_Key to be "descriptionA"
                    component.Search_Key = "descriptionA";
                    component.toDoListItems = testData;
                    fixture.detectChanges();

                    // Get All list Item
                    let LIST_ITEMS = fixture.debugElement.queryAll(By.css("to-do-list-item"));

                    // Items Match the Filter
                    expect(LIST_ITEMS.length).toBe(2);
                    expect(LIST_ITEMS[0].nativeNode.textContent).toContain('descriptionA');
                    expect(LIST_ITEMS[1].nativeNode.textContent).toContain('descriptionA');
                });

                // # To Do Item Lsit : Render items correctlly when both filter is set
                it('# To Do Item Lsit : Render items correctlly when both filter is set', () => {
                    
                    const testData = [{
                        id : "id1",
                        status : false,
                        timestamp : 1,
                        description : "descriptionA",
                        modifyFlag : false
                    },
                    {
                        id : "id3",
                        status : true,
                        timestamp : 3,
                        description : "descriptionB",
                        modifyFlag : false
                    },
                    {
                        id : "id2",
                        status : true,
                        timestamp : 2,
                        description : "descriptionC",
                        modifyFlag : false
                    }];

                    // Set toDoListItems, Search_Key to be "descriptionC" and Search_Status to be "Completed"
                    component.Search_Key = "descriptionC";
                    component.Search_Status = "Completed";
                    component.toDoListItems = testData;
                    fixture.detectChanges();

                    // Get All list Item
                    let LIST_ITEMS = fixture.debugElement.queryAll(By.css("to-do-list-item"));

                    // Items Match the Filter
                    expect(LIST_ITEMS.length).toBe(1);
                    expect(LIST_ITEMS[0].nativeNode.textContent).toContain('descriptionC');
                    expect(LIST_ITEMS[0].nativeNode.id).toBe('id2');
                });

            });

            // # To Do Item Form : Edit Form
            describe(`# To Do Item Form : Edit Form`, () => {

                // # To Do Item Form : should display edit one when modifyFlag is true
                it('# To Do Item Form : should display edit one when modifyFlag is true', () => {
                    
                    // Set toDoListItems having one Item with modifyFlag True 
                    component.toDoListItems = [{
                        id : "id",
                        status : false,
                        timestamp : 333,
                        description : "description",
                        modifyFlag : true
                    }];
                    fixture.detectChanges();

                    // Get Form
                    const FORM = fixture.debugElement.nativeElement.querySelector('to-do-list-item-form.Edit-Form');
                    
                    // Form to be existed
                    expect(FORM).toBeTruthy();
                });

                // # To Do Item Form : should not display edit one when modifyFlag is false
                it('# To Do Item Form : should not display edit one when modifyFlag is false', () => {
                    
                    // Set toDoListItems having one Item with modifyFlag False 
                    component.toDoListItems = [{
                        id : "id",
                        status : false,
                        timestamp : 333,
                        description : "description",
                        modifyFlag : false
                    }];
                    fixture.detectChanges();

                    // Get Form
                    const FORM = fixture.debugElement.nativeElement.querySelector('to-do-list-item-form.Edit-Form');
                    
                    // Form to be not exist
                    expect(FORM).toBeFalsy();
                });
                
            });

        });

    });

    // Function Testing
    describe(`Function Testing`, () => {

        const testData : ToDoListItem = {
            id : "id",
            status : false,
            timestamp : 333,
            description : "description",
            modifyFlag : false
        };

        // Inital the component to be tested
        beforeEach(async () => {
            fixture = TestBed.createComponent(ToDoListComponent);
            FakeService = TestBed.inject(ToDoListService);
            component = fixture.componentInstance;
            fixture.detectChanges();
        });

        // SortingDir() : should return "ASC" when Sorting_Dir is true
        it('# SortingDir() : should return "ASC" when Sorting_Dir is true', () => {

            // set Sorting_Dir is true
            component.Sorting_Dir = true;

            // except SortingDir to be "ASC"
            expect(component.SortingDir).toBe("ASC");

        });

        // SortingDir() : should return "DES" when Sorting_Dir is false
        it('# SortingDir() : should return "DES" when Sorting_Dir is false', () => {

            // set Sorting_Dir is false
            component.Sorting_Dir = false;

            // except SortingDir to be "DES"
            expect(component.SortingDir).toBe("DES");

        });

        // toggleSortingDir() : Click Button to Trigger toggleSortingDir()
        it('# toggleSortingDir() : Click Button to Trigger toggleSortingDir()', () => {

            // Spy On toggleSortingDir in component
            let spy = spyOn(component, 'toggleSortingDir');

            // Get Button
            const BUTTON = fixture.debugElement.nativeElement.querySelector('button.sortByTimeStamp');

            // Button Click
            BUTTON.click();

            // Function Have been call once
            expect(spy).toHaveBeenCalledTimes(1);
        });

        // toggleSortingDir()
        it('# toggleSortingDir() : toggle Sorting_Dir false -> true ', () => {
            
            // Initial the Sorting_Dir to false
            component.Sorting_Dir = false;

            // call first Toggle
            component.toggleSortingDir();

            // Expect True
            expect(component.Sorting_Dir).toBeTrue();
        });

        // toggleSortingDir()
        it('# toggleSortingDir() : toggle Sorting_Dir true -> false ', () => {
            
            // Initial the Sorting_Dir to false
            component.Sorting_Dir = true;

            // call first Toggle
            component.toggleSortingDir();

            // Expect False
            expect(component.Sorting_Dir).toBeFalse();
        });

        // new To Do Item : Click Button to Set newToDoFlag to true
        it('# New To Do Item  : Click Button to Set newToDoFlag to true', () => {

            // initial newToDoFlag to be false
            component.newToDoFlag = false

            // Get Button
            const BUTTON = fixture.debugElement.nativeElement.querySelector('button.newToDoItem');

            // Button Click 
            BUTTON.click();

            // newToDoFlag to be true
            expect(component.newToDoFlag).toBeTrue();
        });

        // deleteToDo()
        it('# deleteToDo() : delete To Do', () => {
            
            // Spy On deleteToDo in Service
            let spy = spyOn(FakeService, 'deleteToDo');

            // Call Function
            component.deleteToDo(testData);

            // Except Call Once with data
            expect(spy).toHaveBeenCalledOnceWith(testData);
        });

        // toggleToDo()
        it('# toggleToDo() : Toggle To Do Status', () => {
            
            // Spy On updateToDo in Service
            let spy = spyOn(FakeService, 'updateToDo');

            // Call Function
            component.toggleToDo(testData);

            // Except Call Once with data
            expect(spy).toHaveBeenCalledOnceWith(testData);
        });

        // updateToDo()
        it('# updateToDo() : Update To Do', () => {
            
            // Spy On updateToDo in Service
            let spy = spyOn(FakeService, 'updateToDo');

            // Call Function
            component.updateToDo(testData);

            // Except Call Once with data
            expect(spy).toHaveBeenCalledOnceWith(testData);
        });

        // createToDo()
        it('# createToDo() : Update To Do', () => {
            
            // initial newToDoFlag to be true
            component.newToDoFlag = true;

            // Spy On createToDo in Service
            let spy = spyOn(FakeService, 'createToDo');

            // Call Function
            component.createToDo(testData);

            // Except Call Once with data
            expect(spy).toHaveBeenCalledOnceWith(testData);

            // Except newToDoFlag to be false
            expect(component.newToDoFlag).toBeFalse();
        });

        // cancelCreatingToDo()
        it('# cancelCreatingToDo() : cancel Creating To Do', () => {
            
            // initial newToDoFlag to be true
            component.newToDoFlag = true;

            // Call Function
            component.cancelCreatingToDo();

            // Except newToDoFlag to be false
            expect(component.newToDoFlag).toBeFalse();
        });

    });

    // Life Hook Testing
    describe(`Life Hook Testing`, () => {

        const testData = [{
            id : "id",
            status : false,
            timestamp : 333,
            description : "description",
            modifyFlag : true
        }];

        // Inital the component to be tested
        beforeEach(async () => {
            fixture = TestBed.createComponent(ToDoListComponent);
            FakeService = TestBed.inject(ToDoListService);
            component = fixture.componentInstance;
        });

        // ngOnInit() : 
        describe(`ngOnInit()`, () => {

            // To Do Data should Sync to service
            it('# should subscribe the Data from Service', () => {

                // Initial Status
                expect(component.toDoListItems).toEqual([]);

                // Run OnInit
                component.ngOnInit();
                
                // Put the test Data Next in service 
                FakeService['currentToDoListSubject'].next(testData);
        
                // To Do Data should get Synced
                expect(component.toDoListItems).toEqual(testData);
            })

        });

        // ngOnDestroy() : 
        describe(`ngOnDestroy()`, () => {

            // To Do Data should Never Sync to service
            it('# should never sync the Data from Service', () => {
                
                // initial
                fixture.detectChanges();

                component.ngOnDestroy();

                // Put the test Data Next in service 
                FakeService['currentToDoListSubject'].next(testData);
        
                // To Do Data should not get Synced
                expect(component.toDoListItems).not.toEqual(testData);
            })

        });

    });

    // Child Component - @Input @Output
    describe(`Child Component - @Input @Output`, () => {

        // Inital the component to be tested
        beforeEach(async () => {
            // Parent
            fixture = TestBed.createComponent(ToDoListComponent);
            component = fixture.componentInstance;
            fixture.detectChanges();
        });

        // To Do List Item
        describe(`ToDoListItemComponent - <to-do-list-item>`, () => {

            let mockChildComponent : ToDoListItemComponent;

            // Inital the component to be tested
            beforeEach(async () => {
                // Set component to render the child
                component.Search_Key = "description";
                component.Search_Status = "";
                component.toDoListItems = [{
                    id : "id1",
                    status : false,
                    timestamp : 1,
                    description : "description",
                    modifyFlag : false
                }];
                fixture.detectChanges();
                
                // Child Component
                const childDirQueryResult = fixture.debugElement.query(By.directive(ToDoListItemComponent));
                mockChildComponent = childDirQueryResult.componentInstance;
                
            });

            // <to-do-list-item> : should create
            it('# <to-do-list-item> : should create', () => {
                expect(mockChildComponent).toBeTruthy();
            });

            // <to-do-list-item> : @Input ToDo should import from parent
            it('# <to-do-list-item> : @input ToDo should import from parent', () => {
                expect(mockChildComponent.ToDo).toEqual(component.toDoListItems[0]);
            });

            // <to-do-list-item> : @Input Search_Key should import from parent
            it('# <to-do-list-item> : @input Search_Key should import from parent', () => {
                expect(mockChildComponent.Key).toBe(component.Search_Key);
            });

            // <to-do-list-item> : @Ouput _toggleToDo should emit Data to parent toggleToDo
            it('# <to-do-list-item> : @Ouput _toggleToDo should emit Data to parent toggleToDo', () => {
                
                // Spy on Parent toggleToDo
                let spy = spyOn(component, "toggleToDo");

                // Child emit 
                mockChildComponent._toggleToDo.emit(mockChildComponent.ToDo);
                
                // Function should be call once with data
                expect(spy).toHaveBeenCalledOnceWith(mockChildComponent.ToDo);
            });

            // <to-do-list-item> : @Ouput _editToDo should emit Data to parent updateToDo
            it('# <to-do-list-item> : @Ouput _editToDo should emit Data to parent updateToDo', () => {
                
                // Spy on Parent updateToDo
                let spy = spyOn(component, "updateToDo");

                // Child emit 
                mockChildComponent._editToDo.emit(mockChildComponent.ToDo);
                
                // Function should be call once with data
                expect(spy).toHaveBeenCalledOnceWith(mockChildComponent.ToDo);
            });

            // <to-do-list-item> : @Ouput _deleteToDo should emit Data to parent deleteToDo
            it('# <to-do-list-item> : @Ouput _deleteToDo should emit Data to parent deleteToDo', () => {
                
                // Spy on Parent deleteToDo
                let spy = spyOn(component, "deleteToDo");

                // Child emit 
                mockChildComponent._deleteToDo.emit(mockChildComponent.ToDo);
                
                // Function should be call once with data
                expect(spy).toHaveBeenCalledOnceWith(mockChildComponent.ToDo);
            });

        });

        // To Do List Item in Edit Inline
        describe(`ToDoListItemFormComponent in Inline Edit- <to-do-list-item-form>`, () => {

            let mockChildComponent : ToDoListItemFormComponent;

            // Inital the component to be tested
            beforeEach(async () => {
                // Set component to render the child
                component.toDoListItems = [{
                    id : "id1",
                    status : false,
                    timestamp : 1,
                    description : "description",
                    modifyFlag : true
                }];
                fixture.detectChanges();
                
                // Child Component
                const childDirQueryResult = fixture.debugElement.query(By.directive(ToDoListItemFormComponent));
                mockChildComponent = childDirQueryResult.componentInstance;
                
            });

            // <to-do-list-item-form> : should create
            it('# <to-do-list-item-form> : should create', () => {
                expect(mockChildComponent).toBeTruthy();
            });

            // <to-do-list-item-form> : @Input ToDo should import from parent
            it('# <to-do-list-item-form> : @input ToDo should import from parent', () => {
                expect(mockChildComponent.ToDo).toEqual(component.toDoListItems[0]);
            });

            // <to-do-list-item-form> : @Ouput _saveToDo should emit Data to parent updateToDo
            it('# <to-do-list-item-form> : @Ouput _saveToDo should emit Data to parent updateToDo', () => {
                
                // Spy on Parent updateToDo
                let spy = spyOn(component, "updateToDo");

                // Child emit 
                mockChildComponent._saveToDo.emit(mockChildComponent.ToDo);
                
                // Function should be call once with data
                expect(spy).toHaveBeenCalledOnceWith(mockChildComponent.ToDo);
            });

            // <to-do-list-item-form> : @Ouput _cancelToDo should emit Data to parent updateToDo
            it('# <to-do-list-item-form> : @Ouput _cancelToDo should emit Data to parent updateToDo', () => {
                
                // Spy on Parent updateToDo
                let spy = spyOn(component, "updateToDo");

                // Child emit 
                mockChildComponent._cancelToDo.emit(mockChildComponent.ToDo);
                
                // Function should be call once
                expect(spy).toHaveBeenCalledOnceWith(mockChildComponent.ToDo);
            });

        });

        // To Do List Item in Create
        describe(`ToDoListItemFormComponent in Create- <to-do-list-item-form>`, () => {

            let mockChildComponent : ToDoListItemFormComponent;

            // Inital the component to be tested
            beforeEach(async () => {
                // Set component to render the child
                component.newToDoFlag = true;
                fixture.detectChanges();
                
                // Child Component
                const childDirQueryResult = fixture.debugElement.query(By.directive(ToDoListItemFormComponent));
                mockChildComponent = childDirQueryResult.componentInstance;
                
            });

            // <to-do-list-item-form> : should create
            it('# <to-do-list-item-form> : should create', () => {
                expect(mockChildComponent).toBeTruthy();
            });

            // <to-do-list-item-form> : @Ouput _saveToDo should emit Data to parent createToDo
            it('# <to-do-list-item-form> : @Ouput _saveToDo should emit Data to parent createToDo', () => {
                
                // Spy on Parent createToDo
                let spy = spyOn(component, "createToDo");

                // Child emit 
                mockChildComponent._saveToDo.emit(mockChildComponent.ToDo);
                
                // Function should be call once
                expect(spy).toHaveBeenCalledOnceWith(mockChildComponent.ToDo);
            });

            // <to-do-list-item-form> : @Ouput _cancelToDo should emit Data to parent cancelCreatingToDo
            it('# <to-do-list-item-form> : @Ouput _cancelToDo should emit Data to parent cancelCreatingToDo', () => {
                
                // Spy on Parent cancelCreatingToDo
                let spy = spyOn(component, "cancelCreatingToDo");

                // Child emit 
                mockChildComponent._cancelToDo.emit();
                
                // Function should be call once
                expect(spy).toHaveBeenCalledTimes(1);
            });

        });


    });

    // Child Component
    describe(`Child Component`, () => {

        // To Do List Item
        describe(`ToDoListItemComponent - <to-do-list-item>`, () => {

            let component: ToDoListItemComponent;
            let fixture: ComponentFixture<ToDoListItemComponent>;
            
            // Render Testing
            describe(`Render Testing`, () => {

                // Inital the component to be tested
                beforeEach(async () => {
                    fixture = TestBed.createComponent(ToDoListItemComponent);
                    component = fixture.componentInstance;
                    fixture.detectChanges();
                });

                // Component should be created
                it('# should create the component', () => {
                    expect(component).toBeTruthy();
                });

                // Component Data initial
                it('# should initial the Data', () => {
                    
                    // Check All Inital value
                    expect(component.ToDo).toBeDefined();
                });

                // Completed Status : Append class "completed" on .bullet if status is true
                it('# Completed Status : Append class "completed" on .bullet if status is true', () => {
                    
                    // set Status to true
                    component.ToDo.status = true;
                    fixture.detectChanges();
                    
                    // Get Status Light
                    const Status = fixture.debugElement.nativeElement.querySelector('.bullet');

                    // Check class list should contain 'completed'
                    expect(Status.classList).toContain('completed');
                });

                // Completed Status : Remove class "completed" on .bullet if status is false
                it('# Completed Status : Remove class "completed" on .bullet if status is false', () => {
                    
                    // set Status to false
                    component.ToDo.status = false;
                    fixture.detectChanges();
                    
                    // Get Status Light
                    const Status = fixture.debugElement.nativeElement.querySelector('.bullet');

                    // Check class list should not contain 'completed'
                    expect(Status.classList).not.toContain('completed');
                });

                // Highlight Text : Wrap span with "text-highlight" class if the description included key
                it('# Highlight Text : should wrap span with "text-highlight" class if description included key', () => {
                    
                    // set Status to true
                    component.ToDo.description = "Hello World!";
                    component.Key = "Hello";
                    fixture.detectChanges();
                    
                    // Get description div
                    const SPAN = fixture.debugElement.queryAll(By.css('.to-do-item-text .text-highlight'));
                    
                    // Check Word Match Array Length
                    expect(SPAN.length).toBe(1);
                    expect(SPAN[0].nativeElement.textContent).toBe('Hello');

                });

            });

            // Function Testing
            describe(`Function Testing`, () => {

                // Inital the component to be tested
                beforeEach(async () => {
                    fixture = TestBed.createComponent(ToDoListItemComponent);
                    component = fixture.componentInstance;
                    fixture.detectChanges();
                });

                // toggleStatus() : Click li to Trigger toggleStatus()
                it('# toggleStatus() : Click li to Trigger toggleStatus()', () => {

                    // Spy On toggleStatus in component
                    let spy = spyOn(component, 'toggleStatus');

                    // Get li
                    const LI = fixture.debugElement.nativeElement.querySelector('li.list-group-item');

                    // li click
                    LI.click();

                    // Function Have been call once
                    expect(spy).toHaveBeenCalledTimes(1);
                });

                // toggleStatus() : Click li prevent trigger editItem() and deleteItem()
                it('# toggleStatus() : Click li prevent trigger editItem() and deleteItem()', () => {

                    // Spy On edit and delete in component
                    let spyedit = spyOn(component, 'editItem');
                    let spydelete = spyOn(component, 'deleteItem');

                    // Get li
                    const LI = fixture.debugElement.nativeElement.querySelector('li.list-group-item');

                    // li click
                    LI.click();

                    // Two Function never been called
                    expect(spyedit).toHaveBeenCalledTimes(0);
                    expect(spydelete).toHaveBeenCalledTimes(0);
                });

                // toggleStatus() : Will Emit the To Do Item with status true (from false)
                it('# toggleStatus() : Will Emit the To Do Item with status true (from false)', () => {

                    // Spy On emit in component._toggleToDo
                    let spy = spyOn(component._toggleToDo, 'emit');

                    // set To Do Data
                    component.ToDo = {
                        id : "id",
                        status : false,
                        timestamp : 333,
                        description : "description",
                        modifyFlag : false
                    };

                    // Get li
                    const LI = fixture.debugElement.nativeElement.querySelector('li.list-group-item');

                    // li click
                    LI.click();

                    // Function Have been call once with status as true
                    expect(spy).toHaveBeenCalledWith({
                        id : "id",
                        status : true,
                        timestamp : 333,
                        description : "description",
                        modifyFlag : false
                    });
                });

                // toggleStatus() : Will Emit the To Do Item with status false (from true)
                it('# toggleStatus() : Will Emit the To Do Item with status false (from true)', () => {

                    // Spy On emit in component._toggleToDo
                    let spy = spyOn(component._toggleToDo, 'emit');

                    // set To Do Data
                    component.ToDo = {
                        id : "id",
                        status : true,
                        timestamp : 333,
                        description : "description",
                        modifyFlag : false
                    };

                    // Get li
                    const LI = fixture.debugElement.nativeElement.querySelector('li.list-group-item');

                    // li click
                    LI.click();

                    // Function Have been call once with status as false
                    expect(spy).toHaveBeenCalledWith({
                        id : "id",
                        status : false,
                        timestamp : 333,
                        description : "description",
                        modifyFlag : false
                    });
                });

                // editItem() : Click Button to Trigger editItem()
                it('# editItem() : Click Button to Trigger editItem()', () => {

                    // Spy On editItem in component
                    let spy = spyOn(component, 'editItem');

                    // Get Button
                    const BUTTON = fixture.debugElement.nativeElement.querySelector('button.btn-edit');

                    // Button click
                    BUTTON.click();

                    // Function Have been call once
                    expect(spy).toHaveBeenCalledTimes(1);
                });

                // editItem() : Click Button prevent trigger toggleStatus()
                it('# editItem() : Click Button prevent trigger toggleStatus()', () => {

                    // Spy On toggleStatus in component
                    let spy = spyOn(component, 'toggleStatus');

                    // Get Button
                    const BUTTON = fixture.debugElement.nativeElement.querySelector('button.btn-edit');

                    // Button click
                    BUTTON.click();

                    // Function never been called
                    expect(spy).toHaveBeenCalledTimes(0);
                });

                // editItem() : Will Emit the To Do Item with modifyFlag true
                it('# editItem() : Will Emit the To Do Item with modifyFlag true', () => {

                    // Spy On emit in component._editToDo
                    let spy = spyOn(component._editToDo, 'emit');

                    // set To Do Data
                    component.ToDo = {
                        id : "id",
                        status : false,
                        timestamp : 333,
                        description : "description",
                        modifyFlag : false
                    };

                    // Get Button
                    const BUTTON = fixture.debugElement.nativeElement.querySelector('button.btn-edit');

                    // Button click
                    BUTTON.click();

                    // Function Have been call once with modifyFlag as true
                    expect(spy).toHaveBeenCalledWith({
                        id : "id",
                        status : false,
                        timestamp : 333,
                        description : "description",
                        modifyFlag : true
                    });
                });

                // deleteItem() : Click Button to Trigger deleteItem()
                it('# deleteItem() : Click Button to Trigger deleteItem()', () => {

                    // Spy On deleteItem in component
                    let spy = spyOn(component, 'deleteItem');

                    // Get Button
                    const BUTTON = fixture.debugElement.nativeElement.querySelector('button.btn-delete');

                    // Button click
                    BUTTON.click();

                    // Function Have been call once
                    expect(spy).toHaveBeenCalledTimes(1);
                });

                // deleteItem() : Click Button prevent trigger toggleStatus()
                it('# deleteItem() : Click Button prevent trigger toggleStatus()', () => {

                    // Spy On toggleStatus in component
                    let spy = spyOn(component, 'toggleStatus');

                    // Get Button
                    const BUTTON = fixture.debugElement.nativeElement.querySelector('button.btn-delete');

                    // Button click
                    BUTTON.click();

                    // Function never been called
                    expect(spy).toHaveBeenCalledTimes(0);
                });

                // deleteItem() : Click Button to Trigger deleteItem()
                it('# deleteItem() : Click Button to Trigger deleteItem()', () => {

                    // Spy On emit in component._deleteToDo
                    let spy = spyOn(component._deleteToDo, 'emit');

                    // Get Button
                    const BUTTON = fixture.debugElement.nativeElement.querySelector('button.btn-delete');

                    // Button click
                    BUTTON.click();

                    // Function Have been call once
                    expect(spy).toHaveBeenCalledTimes(1);
                });

            });

        });

        // To Do List Item
        describe(`ToDoListItemFormComponent - <to-do-list-item-form>`, () => {

            let component: ToDoListItemFormComponent;
            let fixture: ComponentFixture<ToDoListItemFormComponent>;

            // Render Testing
            describe(`Render Testing`, () => {

                // Inital the component to be tested
                beforeEach(async () => {
                    fixture = TestBed.createComponent(ToDoListItemFormComponent);
                    component = fixture.componentInstance;
                    fixture.detectChanges();
                });

                // Component should be created
                it('# should create the component', () => {
                    expect(component).toBeTruthy();
                });

                // Component Data initial
                it('# should initial the Data in the component', () => {
                    
                    // Check All Inital value
                    expect(component.toDoForm).toBeDefined();
                    expect(component.submitted).toBeFalse();
                    expect(component['_formBuilder']).toBeDefined();
                });

                // Component should append class "is-invalid" when submit is true and description is invalid
                it('# should append class "is-invalid" when submit is true and description input is invalid', () => {
                    
                    // Set submit to true
                    component.submitted = true;

                    // Set Description to empty and invalid
                    component.toDoForm.patchValue({description : ""});
                    fixture.detectChanges();
                    
                    // Get Input
                    const INPUT = fixture.debugElement.nativeElement.querySelector('Input');

                    // expect input class list contain is-invalid
                    expect(INPUT.classList).toContain('is-invalid');
                });

            });

            // Form Testing
            describe(`Form Testing`, () => {

                // Inital the component to be tested
                beforeEach(async () => {
                    fixture = TestBed.createComponent(ToDoListItemFormComponent);
                    component = fixture.componentInstance;
                    fixture.detectChanges();
                });

                // Form initial
                it('# Form : should create a form with description', () => {
                    
                    // Check contain all items
                    expect(component.toDoForm.contains('description')).toBeTruthy();
                });

                // Form Data initial
                it('# Form : Data initial', () => {
                    
                    // Check All Inital value
                    expect(component.toDoFormControls['description'].value).toBe('');
                });

                // Description is required
                it('# Form : Description is required', () => {
                    
                    // get description control
                    const descriptionControl = component.toDoFormControls['description'];

                    // Set Description to empty and invalid
                    descriptionControl.setValue("");

                    // except control's invalid is true
                    expect(descriptionControl.invalid).toBeTruthy();
                });

            });

            // Function Testing
            describe(`Function Testing`, () => {

                // Inital the component to be tested
                beforeEach(async () => {
                    fixture = TestBed.createComponent(ToDoListItemFormComponent);
                    component = fixture.componentInstance;
                    fixture.detectChanges();
                });

                // onSubmit() : Click submit button will trigger onSubmit function
                it('# onSubmit() : Click submit button will trigger onSubmit function', () => {

                    // Spy On onSubmit in component
                    let spy = spyOn(component, 'onSubmit');

                    // Get BUTTON
                    const BUTTON = fixture.debugElement.nativeElement.querySelector('button[type=submit]');
                    
                    // BUTTON click
                    BUTTON.click();

                    // Function have been called once
                    expect(spy).toHaveBeenCalledTimes(1);

                });

                // onSubmit() : onSubmit invlid form will set submit flag to true and never trigger emit
                it('# onSubmit() : onSubmit invlid form will set submit flag to true and never trigger emit', () => {

                    // Spy On emit in component._saveToDo
                    let spy = spyOn(component._saveToDo, 'emit');

                    // set submitted to false and form to be invalid
                    component.submitted = false;
                    component.toDoForm.patchValue({description : ""});

                    // trigger onSubmit
                    component.onSubmit();

                    // submitted to be true and emit never call
                    expect(component.submitted).toBeTrue();
                    expect(spy).toHaveBeenCalledTimes(0);
                });

                // onSubmit() : onSubmit valid form will trigger emit, set submit flag to false
                it('# onSubmit() : onSubmit valid form will trigger emit, set submit flag to false', () => {

                    // Spy On emit in component._saveToDo
                    let spy = spyOn(component._saveToDo, 'emit');

                    // set submitted to false and form to be invalid
                    component.submitted = false;
                    component.toDoForm.patchValue({description : "description"});

                    // trigger onSubmit
                    component.onSubmit();

                    // submitted to be false and emit have been called
                    expect(component.submitted).toBeFalse();
                    expect(spy).toHaveBeenCalledTimes(1);
                });

                // onSubmit() : Create will emit with new id and timestamp
                it('# onSubmit() : Create will emit with new id and timestamp', () => {

                    // set To Do to create input
                    component.ToDo = {
                        id: '',
                        status: false,
                        timestamp: 0,
                        description: ''
                    };
                    
                    // set description to valid input
                    component.toDoForm.patchValue({description : "description"});
                    
                    // emit with new id and timestamp
                    component._saveToDo.subscribe((ToDo : ToDoListItem) => {
                        expect(ToDo.id).not.toBe('');
                        expect(ToDo.status).toBeFalse();
                        expect(ToDo.timestamp).toBeGreaterThan(0);
                        expect(ToDo.description).toBe('description');
                    });

                    // trigger onSubmit
                    component.onSubmit();
                    
                });

                // onSubmit() : Edit will emit with same id and timestamp. status to false
                it('# onSubmit() : Create will append new id and timestamp', () => {

                    // Spy On emit in component._saveToDo
                    let spy = spyOn(component._saveToDo, 'emit');

                    // set To Do to edit input
                    component.ToDo = {
                        id: 'id',
                        status: true,
                        timestamp: 123,
                        description: 'description123'
                    };
                    
                    // set description to valid input
                    component.toDoForm.patchValue({description : "description123"});
                    
                    // trigger onSubmit
                    component.onSubmit();

                    // emit with same id and timestamp status to false
                    expect(spy).toHaveBeenCalledOnceWith({
                        id: 'id',
                        status: false,
                        timestamp: 123,
                        description: 'description123'
                    });
                    
                });

                // onSubmit() : Data will filter out and replace the HTML Tag with " "
                it('# onSubmit() : Data will filter out and replace the HTML Tag', () => {

                    // Spy On emit in component._saveToDo
                    let spy = spyOn(component._saveToDo, 'emit');

                    // set To Do to edit input
                    component.ToDo = {
                        id: 'id',
                        status: true,
                        timestamp: 123,
                        description: ''
                    };
                    
                    // set description to valid input
                    component.toDoForm.patchValue({description : "<script>alert()</script>"});

                    // trigger onSubmit
                    component.onSubmit();

                    // emit with same id and timestamp status to false
                    expect(spy).toHaveBeenCalledOnceWith({
                        id: 'id',
                        status: false,
                        timestamp: 123,
                        description: ' alert() '
                    });
                    
                });

                // toDoFormControls() : getter will return toDoFormControls
                it('# toDoFormControls() : getter will return toDoFormControls', () => {

                    // getter to be defined and equal to toDoForm.controls
                    expect(component.toDoFormControls).toBeDefined();
                    expect(component.toDoFormControls).toEqual(component.toDoForm.controls);
                });

                // cancel() : Click cancel button will trigger cancel function
                it('# cancel() : Click cancel button will trigger cancel function', () => {

                    // Spy On cancel in component
                    let spy = spyOn(component, 'cancel');

                    // Get BUTTON
                    const BUTTON = fixture.debugElement.nativeElement.querySelector('button[type=button]');
                    
                    // BUTTON click
                    BUTTON.click();

                    // Function have been called once
                    expect(spy).toHaveBeenCalledTimes(1);

                });

                // cancel() : Cancel will emit _cancelToDo with modifyFlag as false and set submitted to false
                it('# cancel() : Cancel will emit _cancelToDo with modifyFlag as false and set submitted to false', () => {

                    // Spy On cancel in component
                    let spy = spyOn(component._cancelToDo, 'emit');

                    // set To Do to edit input
                    component.ToDo = {
                        id: 'id',
                        status: true,
                        timestamp: 123,
                        description: 'description123',
                        modifyFlag: true
                    };
                    component.submitted = true;
                    
                    // trigger cancel() 
                    component.cancel();

                    // Function have been called once with data and submitted to be false
                    expect(component.submitted).toBeFalse();
                    expect(spy).toHaveBeenCalledOnceWith({
                        id: 'id',
                        status: true,
                        timestamp: 123,
                        description: 'description123',
                        modifyFlag: false
                    })

                });

            });

            // Life Hook Testing
            describe(`Life Hook Testing`, () => {

                // Inital the component to be tested
                beforeEach(async () => {
                    fixture = TestBed.createComponent(ToDoListItemFormComponent);
                    component = fixture.componentInstance;
                });

                // ngOnInit() : 
                describe(`ngOnInit()`, () => {

                    // To Do Data should render to Form
                    it('# should subscribe the Data from Service', () => {
                        
                        // Set Description
                        component.ToDo.description = "description";
                        
                        // Run ngOnInit
                        component.ngOnInit();
                
                        // Data toDoListItems should not sync to Service
                        expect(component.toDoFormControls['description'].value).toBe("description");
                    })

                });

            });
        });
    })

    

});

