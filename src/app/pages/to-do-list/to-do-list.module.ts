/******* Angular Resourse *******/
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

/******* Plug-In Resourse *******/
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

/******* Core Resourse *******/
import { CoreCommonModule } from '@core/common.module';

/******* Component Resourse *******/
import { ToDoListComponent } from "./to-do-list.component";
import { ToDoListItemComponent } from './components/to-do-list-item.component';
import { ToDoListItemFormComponent } from './components/to-do-list-item-form.component';

const routes = [
    {
        path: '',
        component: ToDoListComponent
    }
];

@NgModule({
    declarations: [
        ToDoListComponent,
        ToDoListItemComponent,
        ToDoListItemFormComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        CoreCommonModule,
        NgbModule
    ],
    exports: [
        ToDoListComponent
    ]
})
export class ToDiListModule {}