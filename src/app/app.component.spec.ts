import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';

/******* Module Resourse *******/
import { LayoutModule } from './layout/layout.module';


describe('AppComponent', () => {
  
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  // Set Up For the Testbed
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        // Module Resource
        LayoutModule
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();

  });

  // Inital the component to be tested
  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // Render Testing
  describe(`Render Testing`, () => {
    
    // Component should be created
    it('# should create the component', () => {
      expect(component).toBeTruthy();
    });

    // Component should render layout-header
    it('# should render <layout-header>', () => {
      const layout_header = fixture.nativeElement.querySelector('layout-header');
      expect(layout_header).toBeTruthy();
    });

    // Component should render layout-content
    it('# should render <layout-content>', () => {
      const layout_content = fixture.nativeElement.querySelector('layout-content');
      expect(layout_content).toBeTruthy();
    });

  });

});
