import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HeaderComponent } from './header.component';


describe('HeaderComponent - <layer-header>', () => {
  
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  // Set Up For the Test
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
      ],
      declarations: [
        HeaderComponent
      ],
    }).compileComponents();

  });

  // Inital the component to be tested
  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // Render Testing
  describe(`Render Testing`, () => {
    
    // Component should be created
    it('# should create the component', () => {
        expect(component).toBeTruthy();
    });

  });

});
