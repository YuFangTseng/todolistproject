
/******* Angular Resourse *******/
import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'layout-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HeaderComponent implements OnInit, OnDestroy{
  
    // Public

    // Private

    /**
     * Constructor
     */
    constructor() {}

    //  Accessors
    // -----------------------------------------------------------------------------------------------------

    // Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {}

    /**
     * On Destroy
     */
    ngOnDestroy(): void {}

    // Private methods
    // -----------------------------------------------------------------------------------------------------

    // Public methods
    // -----------------------------------------------------------------------------------------------------

}   
