/******* Angular Resourse *******/
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

/******* Plug-In Resourse *******/
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

/******* Core Resourse *******/
import { CoreCommonModule } from '@core/common.module';

/******* Components Resourse *******/
import { HeaderComponent } from './header/header.component';
import { ContentComponent } from './content/content.component';


@NgModule({
    declarations: [
        HeaderComponent,
        ContentComponent
    ],
    imports: [
        CoreCommonModule,
        RouterModule,
        NgbModule
    ],
    exports: [
        HeaderComponent,
        ContentComponent
    ]
})
export class LayoutModule {}
