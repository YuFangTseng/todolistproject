
/******* Angular Resourse *******/
import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'layout-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ContentComponent implements OnInit, OnDestroy{
  
    // Public

    // Private

    /**
     * Constructor
     */
    constructor() {}

    //  Accessors
    // -----------------------------------------------------------------------------------------------------

    // Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {}

    /**
     * On Destroy
     */
    ngOnDestroy(): void {}

    // Private methods
    // -----------------------------------------------------------------------------------------------------

    // Public methods
    // -----------------------------------------------------------------------------------------------------

}   
