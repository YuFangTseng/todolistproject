import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ContentComponent } from './content.component';


describe('ContentComponent - <layer-content>', () => {
  
  let component: ContentComponent;
  let fixture: ComponentFixture<ContentComponent>;

  // Set Up For the Test
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
      ],
      declarations: [
        ContentComponent
      ],
    }).compileComponents();

  });

  // Inital the component to be tested
  beforeEach(() => {
    fixture = TestBed.createComponent(ContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // Render Testing
  describe(`Render Testing`, () => {
    
    // Component should be created
    it('# should create the component', () => {
        expect(component).toBeTruthy();
    });

    // Component should contain router-outlet
    it('# should contain <router-outlet>', () => {
        const router_outlet = fixture.nativeElement.querySelector('router-outlet');
        expect(router_outlet).toBeTruthy();
    });

  });

});
