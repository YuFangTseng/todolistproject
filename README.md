# To-Do-List

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.2.3.

## Development environment

This project is developed under `npm -version : 8.1.2` and `node -version : v16.13.1`

## Development server

Run `npm run start` for a dev server. Will automatically open the browser and navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Build fo production

Run `npm run build:pro` to build the project. The build artifacts will be stored in the `dist/frontend` directory with production configuration.

## Running unit tests

Run `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Docker

### This project provide the Docker File (Dockerfile) for docker build on your local side.

- The project can run the unit test during docker build. Please Uncomment the line.
- The code will run `npm run build:pro` to build up the static file and run on the nginx with port 80.
- Build and Run command will like below :
  1. docker build . -t project:to_do_list
  2. docker run -p 80:80 project:to_do_list
  3. `http://127.0.0.1/` will navigate to the app.

## Codebase Architecture

### src/
  - api : All API services.
  - core : All sharing resource and module including "animation", "component", "const", "pipes", "directives", "service", "helper" and "type".
  - app : All "Page" component or "Layout" component. Each component might have "html", "style file(scss)", "component file" and "module". 
  - assets : All media resource such as "images" and "fonts"  
  - environments : Environments config files folder. Will apply when build up the project. Can set API URL or other var might effect by environment 
  
### src/api/
  API module file and all api service files.

### src/core/
  - animation : Angular animation file
  - component : Sharing component resorece
  - const : const variable file
  - pipes : pipe files
  - directives : directive files
  - service : service file
  - helper : service file with special function like "CanActivate" service or "HttpInterceptor" service
  - type : data type file like interface

### Testing spec file

Will come along with the file to be tested 