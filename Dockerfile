# Step 1. Node image as builder
FROM node:16.16.0-alpine3.16 as builder

# add chromium
# RUN apk add chromium # Enable when need to test on Docker Build

# Assign work folder
WORKDIR /usr/app

# Set ENV CHROME_BIN
# ENV CHROME_BIN=/usr/bin/chromium-browser # Enable when need to test on Docker Build

# only copy package.json
COPY ./package*.json ./

# install all dependencies
RUN npm install

# copy all others
COPY ./ ./

# copy src folder
COPY src src

# run the unit test
# RUN npm run test # Enable when need to test on Docker Build

# build the static file command and output into dist/frontend
RUN npm run build:prod

# pull nginx image
FROM nginx:alpine

# copy static file from step 1.
COPY --from=builder /usr/app/dist/frontend /usr/share/nginx/html

# Override the nginx server config setting
COPY ./nginx-custom.conf /etc/nginx/conf.d/default.conf